@echo OFF
set nipkg="%SystemDrive%\Program Files\National Instruments\NI Package Manager\nipkg.exe"
set FeedName=PetranWay - Temp NIPKG Feed

@echo This script will
@echo 1. Delete the old "%FeedName%" from NI Package Manager if it exists (ignore errors if any).
@echo 2. Delete the "%temp%\%FeedName%" directory if it exists (ignore errors if any).
@echo 3. Create a new "%temp%\%FeedName%".
@echo 4. Create an NI Package Manager feed called "%FeedName%" that contains all of the NI Packages in the folder where this bat file was executed from.
@echo 5. You'll then be instructed with the next set of actions to take. When you ready to continue, press any key.

%SystemRoot%\System32\timeout.exe /T 100

%nipkg% feed-remove "%FeedName%"
rmdir /s /q "%temp%\%FeedName%"
mkdir "%temp%\%FeedName%"
%nipkg% feed-create "%temp%\%FeedName%" "%CD%" 
%nipkg% feed-add "%temp%\%FeedName%" --name="%FeedName%"
@echo  ---------------------------------------------------
@echo  ---------------------------------------------------
@echo 1. Open NI Package manager (if it was already open, select NI Package Manger and press F5 to refresh). 
@echo 2. Navigate to the "Packages" tab and set the "Maintainer" to PetranWay.
@echo 3. Select all packages to install. Accept licensing agreements. 
@echo 4. Once NI Package Manager completes the installations press any key to continue.
%SystemRoot%\System32\timeout.exe /T 500
%nipkg% feed-remove "%FeedName%"
rmdir /s /q "%temp%\%FeedName%"
